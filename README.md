# antiX23 desktop-files

## Description
The antix23-desktop-files package contains translated freedesktop.org .desktop files for antiX 23.

## Support
www.antixforum.com

## Contributing
Please visit https://app.transifex.com/anticapitalista/antix-development/antix23-desktop-files to improve translations in your language.

## Authors and acknowledgment
This is an antiX community project.

## License
GPL Version 3 (GPLv3)

-----------
Written 2023 by Robin.antiX for antiX community.
